FROM python:3.8-slim-buster

ENV HELM_VERSION v3.4.2
ENV PATH $PATH:/root/bin:/root/google-cloud-sdk/bin
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

RUN apt-get update && \
    apt install --yes ash curl git cargo perl libyaml-perl \
    libhash-merge-simple-perl jq docker.io vim \
    bash-completion tmux

WORKDIR /root
RUN mkdir bin
RUN python3 -mensurepip && pip3 install --upgrade pip wheel setuptools
RUN pip3 install --upgrade pip wheel setuptools \
    docker-compose==1.29.2 \
    fabric==2.5.0 \
    pyyaml==5.3.1 \
    requests==2.24.0 \
    grpcio==1.30.0 \
    black==19.10b0 \
    mypy~=0.910 \
    pdm~=1.7.0 \
    twine~=3.4.2

RUN (curl -L https://sdk.cloud.google.com | bash -s -- --disable-prompts) && \
    gcloud components install core beta bq cloud_sql_proxy docker-credential-gcr gcloud kustomize kubectl && \
    ln -s $(which envsubst) bin/envsubst

# Install helm
RUN (curl -L https://get.helm.sh/helm-${HELM_VERSION}-linux-amd64.tar.gz -o - | tar -xzO linux-amd64/helm) > bin/helm  && \
    chmod a+x bin/helm && \
    helm plugin install https://github.com/viglesiasce/helm-gcs.git --version v0.2.0

# Install sops
RUN curl -L https://github.com/mozilla/sops/releases/download/v3.5.0/sops-v3.5.0.linux -o bin/sops

# Install yq for yaml and toml editing
RUN curl -L https://github.com/mikefarah/yq/releases/download/v4.11.2/yq_linux_amd64 -o bin/yq

# Install stern for tailing pods
RUN curl -L https://github.com/wercker/stern/releases/download/1.11.0/stern_linux_amd64 -o bin/stern

ADD bin/* ./bin/
RUN ln -s $(which kubectl) bin/k && chmod -R a+x bin
RUN echo 'source /usr/share/bash-completion/bash_completion' >> ~/.bashrc && \
    echo 'source <(kubectl completion bash)' >> ~/.bashrc && \
    echo 'alias k=kubectl' >> ~/.bashrc && \
    echo 'complete -F __start_kubectl k' >> ~/.bashrc
