#!/usr/bin/env bash

GCLOUD_CONFIG="./gcloud_config"
UNIQUE_NAME=$(uuidgen | cut -d- -f1)

podman build . --tag console
echo "Configuring gcloud authentication..."
podman run --name console-${UNIQUE_NAME} -ti console bash -c "gcloud auth login" -d
echo ""
echo "Copying gcloud config to local directory..."
podman cp console-${UNIQUE_NAME}:/root/.config/gcloud $GCLOUD_CONFIG
echo ""
echo "Cleaning up..."
podman rm console-${UNIQUE_NAME}
