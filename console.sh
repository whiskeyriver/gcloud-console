#!/usr/bin/env bash

GCLOUD_CONFIG="./gcloud_config"
UNIQUE_NAME=$(uuidgen | cut -d- -f1)

set -x
podman run --name console-${UNIQUE_NAME} -v $GCLOUD_CONFIG:/root/.config/gcloud:Z -ti console bash
