#!/usr/bin/env bash


function findport() {
    for i in $(seq 5);
    do
        HOSTPORT=$(netstat -tlp 2> /dev/null | grep $PID | awk '{ print $4 }')
        if [ -z "$HOSTPORT" ];
        then
            sleep 1;
            continue
        fi
        break
    done    
}

GCLOUD_CONFIG="./gcloud_config"
UNIQUE_NAME=$(uuidgen | cut -d- -f1)
CONTAINER="cloud_sql_proxy-${UNIQUE_NAME}"

conn=$1
if [ -z "$conn" ];
then
    echo "You must provide an instance."
    exit 1
fi

port=${2:-"0"}
cmd="cloud_sql_proxy -instances $1=tcp:$port"
echo "${CONTAINER}: Launching $cmd"

podman run -d --name $CONTAINER -v $GCLOUD_CONFIG:/root/.config/gcloud:Z -ti --net=host console bash -c "$cmd"

PID=$(podman inspect -f '{{.State.Pid}}' $CONTAINER)

if [ "$port" -eq "0" ]
then
    echo -n "Finding the attached port for ${CONTAINER}..."
    findport
    echo " $HOSTPORT"
else
    HOSTPORT="localhost:${port}"
fi

if [ -z "$HOSTPORT" ];
then
    echo "Could not find a port for ${PID}."
    podman attach $CONTAINER
    exit 1
fi

echo -n "Username: "
read username
echo -n "Password: "
read password
echo -n "Database: "
read database

psql postgresql://${username}:${password}@$HOSTPORT/${database}

echo -n "Deleting cloud_sql container: "
podman kill $CONTAINER